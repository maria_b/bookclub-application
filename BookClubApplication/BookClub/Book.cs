using System;

namespace BookClub
{
    ///<summary>
    /// Book classes defines a Book object that is inside the BookClub project.
    ///</summary>
    class Book
    {
        // Property of the Book class allowing to get and set the id of a Book
        public int BookId {get; set;}
        // Property of the Book class allowing to get and set the title of a Book
        public String Title {get; set;}
        // Property of the Book class allowing to get and set the description of a Book
        public String Description {get; set;}
        // Property of the Book class allowing to get and set the genre of a Book
        public String Genre {get; set;}
        // Property of the Book class allowing to get and set the last name of the 
        // author for a Book
        public String AuthorLastName {get; set;}
        // Property of the Book class allowing to get and set the first name of the 
        // author for a Book
        public String AuthorFirstName {get; set;}
        // Property of the Book class allowing to get and set the rating for a Book
        public double Rating {get;set;}
        // Property of the Book class allowing to get and set the number of readers for a Book
        public int NumReaders{get;set;}

    }
}