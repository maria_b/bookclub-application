﻿using System;
using System.Collections.Generic;
namespace BookClub
{
    ///<summary>
    /// Program class creates a BookClubApplication objects, loads data from the two 
    ///provided xml files and runs the BookClubApplication object with the data from files.
    ///</summary>
    class Program
    {
        static void Main(string[] args)
        {
            //Create a BookClubApplication object
            BookClubApplication app1 = new BookClubApplication();
            //Create a list of Book from the two xml resouces files
            List<Book> bookList = app1.LoadData("BookClub/Resources/books.xml","BookClub/Resources/ratings.xml");
            //Run the BookClubApplication object
            app1.Run(bookList);
        }
    }
}
