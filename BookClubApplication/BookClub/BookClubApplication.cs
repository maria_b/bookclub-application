using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
namespace BookClub
{
    ///<summary>
    /// BookClubApplication class holds all the functionality of BookClubApplication.
    /// It has a constructor, a method to load the data from the XML files , a method
    /// to run the BookClubApplication and various helper methods for the game menu 
    /// functionality.
    ///</summary>
    class BookClubApplication
    {
        //A Property of List<Book> to hold the data
        // of Book objects based on the the input of two XML files.
        private List<Book> bookObjects{get; }
        // A property of bool to keep track if the application was stopped by the user
        private static bool IsRunning {get; set; }
        ///<summary>
        /// BookClubApplication sets initially the BookClubApplication IsRunning property
        /// to true to indicate that the program was started.
        /// or not
        ///</summary>
        public BookClubApplication(){
            IsRunning = true;
        }
        ///<summary>
        /// LoadData reads data from the two input XML files and performs
        /// link operations on it to create one single list of books.
        ///</summary>
        ///<param name=“filePathBooks”>Relative path to books.xml </param>
        ///<param name=“filePathRatings”>Relative path to ratings.xml</param>
        ///<returns> A List<Books> containing data from the two input XML files</returns>
        public  List<Book> LoadData(String filePathBooks , String filePathRatings){
            //Load books.xml
            var booksXml = XElement.Load(filePathBooks);
            //Get all book descendants
            var allbooksXml = booksXml.Descendants("book"); 
            //Select the id attribute, the title element, the description element,
            //the genre element, the authorlastname attribute and the authorfirstname 
            // attribute for each book
            var allBooks = from book in allbooksXml
            select new {
                BookId = int.Parse(book.Attributes("id").First().Value), 
                Title = book.Element("title").Value,
                Description = book.Element("description").Value,
                Genre = book.Element("genre").Value,
                AuthorLastName = book.Element("author").Attribute("lastName").Value ,
                AuthorFirstName = book.Element("author").Attribute("firstName").Value
            };
            //Get all data from ratings.xml
            var ratingsXml = XElement.Load(filePathRatings);
            //Get all book descendants of ratings.xml
            var allratingsXml = ratingsXml.Descendants("book"); 
            //Select the id attribute, the rating average, and the number of readers for each book
            var allRatings = from rating in allratingsXml
            select new {
                BookId = int.Parse(rating.Attributes("id").First().Value), 
                AvgRating = rating.Elements("rating").Average(r=>Convert.ToInt32(r.Value)),
                NumReaders = rating.Elements("rating").Count()
            };
            //perform a JOIN operation on two IEnumerable of anonymous type (allBooks and allRatings)
            //where the id of a book should be equal. Create a new IEnumerable anonymous type containing
            //combined data from allBooks and allRatings
            var joinedData =from book in allBooks
	        join rating in allRatings on book.BookId equals rating.BookId
            select new {
                BookId = book.BookId,
                Title = book.Title,
                Description = book.Description,
                Genre = book.Genre,
                AuthorLastName = book.AuthorLastName,
                AuthorFirstName = book.AuthorFirstName,
                Rating = rating.AvgRating,
                NumReaders = rating.NumReaders    
            };
            //Create a bookObjectsList of  List<Book>
            List<Book> bookObjectsList = new List<Book>();
            //Loop through each item of joined data and add it to the list of Book
            //making sure that the data is stored in properties of a Book type
            foreach(var item  in joinedData ){
                Book bookObj = new Book();
                bookObj.BookId = item.BookId;
                bookObj.Title = item.Title;
                bookObj.Description = item.Description;
                bookObj.Genre = item.Genre;
                bookObj.AuthorLastName = item.AuthorLastName;
                bookObj.AuthorFirstName = item.AuthorFirstName;
                bookObj.Rating = Math.Truncate(item.Rating*100)/100;
                bookObj.NumReaders = item.NumReaders;
                bookObjectsList.Add(bookObj);  
            }
            return bookObjectsList;
        }
        ///<summary>
        /// Run method uses the input List<Book> containing all data relevant to the program
        /// and displays a menu to the user asking the user to choose what kind of operation
        /// the user wishes to perform on the input data.
        ///</summary>
        ///<param name=“bookList”>Data retrieved from books.xml and ratings.xml </param>
        public void Run(List<Book> bookList){
            // While the application is still running and has not been exited by the user,
            // display the menu and ask the user to make a choice. 
            while(IsRunning){
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("| **---Welcome to the reader's nook book club !---** |");
            Console.WriteLine("| What would you like to see ? :                     |");
            Console.WriteLine("| 1) View the top-rated books                        |");
            Console.WriteLine("| 2) Browse books by genre                           |");
            Console.WriteLine("| 3) Search book by keyword                          |");
            Console.WriteLine("| 4) Display books in my favorite genre              |");
            Console.WriteLine("| 5) Exit the program                                |");
            Console.WriteLine("------------------------------------------------------");
            Console.Write("Select an option:");
           
            //Continiously read the user input as the program is running
            switch (Console.ReadLine())
            {
                case "1":
                    //Call the ViewTopRatedBooks  to diplay top 5 most popular books
                    ViewTopRatedBooks(bookList);
                    break;
                case "2":
                    //Call the BrowsePopularBooks to browse books by popular genre
                    BrowsePopularBooks(bookList);
                    break;
                case "3":
                    //Call SearchBook to search for a book using a keyword
                    SearchBook(bookList);
                    break;
                case "4":
                    //Call BrowseWithSuprise to browse books using a suprise condition
                    BrowseWithSuprise(bookList);
                    break;
                case "5":
                    //Print goodbye and set the program as no longer running forcing the program
                    // to stop running
                    Console.WriteLine("Goodbye !");
                    IsRunning = false;
                    break;
                default:
                    //By default, if the user does not input a number that is between (1-5),
                    // the program automatically exits
                    Console.WriteLine("Goodbye !");
                    IsRunning = false;
                    break;
            }
            }
        
        }
        ///<summary>
        /// ViewTopRatedBooksmethod allows the user to view the top 5 most popular books.
        ///</summary>
        ///<param name=“bookList”>Data retrieved from books.xml and ratings.xml </param>
        private void ViewTopRatedBooks(List<Book> bookList){
            //Perform a LINQ query to find the top 5 most popular books according to 
            // the fact that a popular book has many users and a high average rating.
            var resultingTop5Popularbooks = (from book in bookList
                                        orderby book.NumReaders descending, book.Rating descending
                                        select book).Take(5);
             Console.WriteLine(" TOP 5 BOOKS IN ALL GENRES "); 
             //Loop through the result to display it to the user  
             foreach(var bookObj  in resultingTop5Popularbooks ){
                Console.WriteLine();
                Console.WriteLine(" Title : " + bookObj.Title + "\n"+ " Description : "+ bookObj.Description + "\n"+ " Genre : "
                + bookObj.Genre + " Author : " +bookObj.AuthorLastName +" "+ bookObj.AuthorFirstName+ "\n"+" Average Rating : "+bookObj.Rating + " Number readers : "+ bookObj.NumReaders);        
            }
        }
        ///<summary>
        /// BrowsePopularBooks allows the user to browse books according to ppular genre.
        ///</summary>
        ///<param name=“bookList”>Data retrieved from books.xml and ratings.xml </param>
        private void BrowsePopularBooks(List<Book> bookList){
            //Perform a LINQ query to first order the data by the highest number of readers, the highest average 
            // rating and then group the data by book genre 
            var resultingPopularbooks = from book in bookList
                                        orderby book.NumReaders descending, book.Rating descending
                                        group book by book.Genre into groupedByGenre
                                        select groupedByGenre;
            Console.WriteLine(" POPULAR BOOKS BY GENRE ");
            //loop through the collection of IGrouping and print the genre of each book, the title, the number of books
            // in the genre and the average rating of the genre
            foreach(var group  in resultingPopularbooks ){
                Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                Console.WriteLine(" -- Book Genre : " + group.Key + " Book Count in this Genre : " + group.Count() +  "" +" Average rating : " + Math.Truncate(group.Select(b=>b.Rating).Average()*100)/100);
                Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                //Display each book in the genre
                foreach(var book in group ){
                    Console.WriteLine( " ->" + book.Title );
                }
            }
            

        }
        ///<summary>
        /// SearchBook allows the user to search for a book using a keyword either by title or description.
        ///</summary>
        ///<param name=“bookList”>Data retrieved from books.xml and ratings.xml </param>
        private void SearchBook(List<Book> bookList){
            Console.WriteLine("SEARCH BOOK BY KEYWORD ");
            Console.WriteLine("Enter the keyword : ");
            var readValue = Console.ReadLine();
            //Perform a LINQ query only selecting books that have the input keyword by the user
            //in the title and in the description , the case of the input word does not affect the search
            // the word is converted to lowercase or the first letter is converted to uppercase
            var resultingBook = bookList.Where(
                b => b.Title.Contains(readValue.ToString().ToLower()) || b.Title.Contains(char.ToUpper(readValue[0]) + readValue.Substring(1).ToLower())
                || b.Description.Contains(readValue.ToString().ToLower()) || b.Description.Contains(char.ToUpper(readValue[0]) + readValue.Substring(1).ToLower()));
            Console.WriteLine();
                //Loop through the book objects and display them to the user 
                foreach(var bookObj  in resultingBook ){
                    Console.WriteLine(" Title : " + bookObj.Title + "\n"+ " Description : "+ bookObj.Description + "\n"+ " Genre : "
                    + bookObj.Genre + " Author : " +bookObj.AuthorLastName +" "+ bookObj.AuthorFirstName+ "\n"+" Average Rating : "+bookObj.Rating + " Number readers : "+ bookObj.NumReaders);        
                }
            
        }
        ///<summary>
        /// BrowseWithSuprise allows the user to display books using a suprise condition which is
        /// actually the top 5 rated books in my favorite genre : Fantasy.
        ///</summary>
        ///<param name=“bookList”>Data retrieved from books.xml and ratings.xml </param>
        private void BrowseWithSuprise(List<Book> bookList){
            //Perform a LINQ  query to selct books in the fantasy genre,
            //ordered by the highest rating ,and only the top 5 books should be selcted
            var resultingBooks = (from book in bookList
                                        where book.Genre.Contains("Fantasy") 
                                        orderby book.Rating descending
                                        select book).Take(5);
             Console.WriteLine(" 5 GREATEST BOOKS IN MY FAVORITE GENRE : FANTASY ");  
             //Loop through the result of the query and display the books to the user 
             foreach(var bookObj  in resultingBooks ){
                Console.WriteLine();
                Console.WriteLine(" Title : " + bookObj.Title + "\n"+ " Description : "+ bookObj.Description + "\n"+ " Genre : "
                + bookObj.Genre + " Author : " +bookObj.AuthorLastName +" "+ bookObj.AuthorFirstName+ "\n"+" Average Rating : "+bookObj.Rating + " Number readers : "+ bookObj.NumReaders);        
            }

        }
       
    }
    
}